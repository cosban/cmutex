module gitlab.com/cosban/cmutex

go 1.14

require (
	github.com/cosban/assert v1.1.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
)
