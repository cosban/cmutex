package cmutex

import "context"

// Mutex is similar in functionality to sync.Mutex except in that it has the ability to cancel a lock attempt if a
// given context is cancelled.
type Mutex interface {
	// Lock is a blocking function which attempts to lock the mutex
	Lock()
	// Unlock will unlock a locked mutex
	Unlock()
	// CLock is a blocking function which attempts to lock the mutex but will cancel that attempt if the provided
	// context is canceled. CLock returns true if the mutex was successfully locked, and false if it was abandoned
	CLock(context.Context) bool
}
