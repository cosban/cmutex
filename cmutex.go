package cmutex

import "context"

type cmutex struct {
	m chan bool
}

// New returns a mutex which is initially unlocked
func New() Mutex {
	cm := &cmutex{
		m: make(chan bool, 1),
	}
	cm.m <- true
	return cm
}

// NewLocked returns a mutex which is initially locked
func NewLocked() Mutex {
	return &cmutex{
		m: make(chan bool, 1),
	}
}

func (cm *cmutex) Lock() {
	<-cm.m
}

func (cm *cmutex) Unlock() {
	cm.m <- true
}

func (cm *cmutex) CLock(context context.Context) bool {
	select {
	case <-cm.m:
		return true
	case <-context.Done():
		return false
	}
}
