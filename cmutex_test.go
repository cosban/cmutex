package cmutex

import (
	"testing"
	"time"

	"github.com/cosban/assert"
	"golang.org/x/net/context"
)

func TestLock(t *testing.T) {
	given := New()
	given.Lock()
}

func TestUnlock(t *testing.T) {
	given := NewLocked()
	given.Unlock()
}

func TestCLock(t *testing.T) {
	given := New()

	actual := given.CLock(context.Background())
	assert.True(t, actual)
}

func TestAbandonsIfContextCancelled(t *testing.T) {
	given := NewLocked()

	ctx, abandon := context.WithCancel(context.Background())

	go func() {
		time.Sleep(1 * time.Second)
		abandon()
	}()

	actual := given.CLock(ctx)
	assert.False(t, actual)
}

func TestNoDeadlockWhenAbandoned(t *testing.T) {
	given := NewLocked()

	ctx, abandon := context.WithCancel(context.Background())

	go func() {
		time.Sleep(1 * time.Second)
		abandon()
	}()

	first := given.CLock(ctx)
	assert.False(t, first)

	given.Unlock()
	actual := given.CLock(context.Background())
	assert.True(t, actual)
}
